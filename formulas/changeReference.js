/**
 * 
 * @param {*} seriesReference required, first series as reference for result. Default expectation: time series with values (x-values) beeing mapped to depth (y-values).
 * @param {*} seriesValues  required, second series containing actual values. Default expectation: time series with values (x-values) beeing mapped to value (y-values).
 * @param {function} grouping Optional, function to apply on seriesValues grouped by seriesReference (default: y-Values for identical x-Values). Reduces x-Value to unique value list, groups y-Values by given function
 * @param {*} options Optional, can override any aspect of the executed function: 
 *  fieldReference: Default "y", defines which field to be read from seriesReference;
 *  fieldValue: Default "y", defines which field to be read from seriesValues;
 *  castReference: function or declaration on how to cast the seriesReference values before working on them;
 *  castValue: function or declaration on how to cast the seriesValues values before working on them;
 *  targetFieldReference: Default "x", defines where to store the seriesReference value in result item;
 *  targetFieldValue: Default "y", defines where to store the seriesValues value in result item;
 *  lookupOptions: If set, both series are prepared by looking up and matching their values. Check _lookup for details of the options. Lookup does not happen, if this option does not exist.
 * @returns seriesReference input type with combined values.
 */
function changeReference(
    seriesReference,
    seriesValues,
    grouping,
    {
        fieldReference,
        fieldValue,
        castReference,
        castValue,
        targetFieldReference,
        targetFieldValue,
        lookupOptions
    } = {
            fieldReference: "y",
            fieldValue: "y",
            targetFieldReference: "x",
            targetFieldValue: "y"
        }) {

    if (debug) {
        console.log("changeReference a: " + JSON.stringify(seriesReference));
        console.log("changeReference b: " + JSON.stringify(seriesValues));
    }

    // Normalize input data, user default fallbacks
    seriesReference = _fetchData(seriesReference);

    let referenceArray = toValueArray(seriesReference, fieldReference, castReference);
    let valueArray = toValueArray(seriesValues, fieldValue, castValue);

    // Check if value and reference should be combined by looking up values in sources. Manipulate values to be combined accordingly
    if (lookupOptions) {
        let { refArray, valArray } = _lookup(seriesReference,
            seriesValues,
            lookupOptions
        )

        referenceArray = refArray;
        valueArray = valArray;
    }

    // Combine series as configured
    let combinedSeries = referenceArray.map((reference, index) => {
        return {
            [targetFieldReference]: reference,
            [targetFieldValue]: valueArray[index]
        }
    });

    // if grouping function is given, apply grouping by reference
    if (_isFunction(grouping)) {
        combinedSeries = _groupArrayByReference(combinedSeries, grouping, targetFieldReference, targetFieldValue);
    }

    return Object.assign(seriesReference, {
        values: combinedSeries
    });
}

/**
 * Prepares the input serieses to be combined by looking up and comparing values of specified compare fields.
 * For example, if combining two unsorted time serieses, lookup can sort them by their timestamps accordingly. Or it can match a list of events via timestamp with a time series.
 * @param {*} seriesReference 
 * @param {*} seriesValues 
 * @param {Object} lookupOptions Options to be applied on this function:
 *  lookupFieldReference: Default "x". Define the field of the seriesReference to be taken for comparison.
 *  lookupFieldValue: Default "x". Define the field of the seriesValues to be taken for comparison.
 *  lookupReferenceCast: function or declaration on how to cast the seriesReference values before working on them;
 *  lookupValueCast: function or declaration on how to cast the seriesValues values before working on them;
 *  lookupFunction: Default compares if values are identical {(a, b) => a == b}, any function that accepts the two values and returns a boolean, if they should be combined. First input is always reference, second is value.
 * @returns Object holding the manipulated and sorted arrays to be combined
 */
function _lookup(
    seriesReference,
    seriesValues,
    {
        lookupFieldReference,
        lookupFieldValue,
        lookupReferenceCast,
        lookupValueCast,
        lookupFunction
    } = {
            lookupFieldReference: "x",
            lookupFieldValue: "x",
            lookupFunction: (a, b) => a == b
        }) {
    let refSource = toValueArray(seriesReference, lookupFieldReference, lookupReferenceCast);
    let valSource = toValueArray(seriesValues, lookupFieldValue, lookupValueCast);

    // refArray: [{1,200}, {2,200}, {3,300}]
    // valArray: [{4,50},{3,1},{2,-4}]
    // Reduce reference array to values, where the lookup values match, Keep original order
    let refIndices = refSource.reduce((indices, reference, index) => {
        if (valSource.filter(value => lookupFunction(reference, value)).length > 0) {
            indices.push(index);
        }
        return indices;
    }, []);
    refSource = refSource.filter((value, index) => refIndices.includes(index));
    refArray = referenceArray.filter((value, index) => refIndices.includes(index));
    // -> refArray: [{2,200}, {3,300}]

    let valIndices = valSource.reduce((indices, value, index) => {
        let matchesAt = refSource.filter(reference => lookupFunction(reference, value)).reduce((reference, index) => index);
        if (matchesAt) {
            indices.push(matchesAt);
        }
        return indices;
    }, [])
    // -> [{3,1}, {2,-4}]

    valArray = valArray
        .filter((value, index) => valIndices.includes(index))
        .reduce((resultArray, value, index) => {
            resultArray[valIndices[index]] = value;
            return resultArray;
        }, Array(refIndices.length));
    // -> [{2, -4}, {3,1}]

    return {
        refArray,
        valArray
    }
}

function _groupArrayByReference(array, grouping, refField, valueField) {
    let groups = array
        .sort((a, b) => a[refField] - b[refField])
        .reduce((groupedSeries, element) => {
            let ref = element[refField];
            let val = element[valueField];
            if (groupedSeries[ref] === undefined) {
                groupedSeries[ref] = [];
            }
            groupedSeries[ref].push(val);

            return groupedSeries;
        }, {});

    return Object.entries(groups).map(group => {
        let ref = group[0];
        let values = group[1];
        return {
            [refField]: ref,
            [valueField]: grouping(values)
        }
    })
}

function sum(array) {
    return array.reduce((sum, value) => sum += value, 0);
}

function max(array) {
    return array.reduce(Math.max);
}

function min(array) {
    return array.reduce(Math.min);
}

function first(array) {
    return array[0];
}