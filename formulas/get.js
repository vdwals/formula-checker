function get(input, field, cast) {
    const data = _fetchData(input);

    if (_isArray(data.values)) {
        return Object.assign(data,
            {
                values: data.values
                    .map(pair => {
                        if (field !== undefined) {
                            return {
                                [field]: _cast(pair[field], cast)
                            };
                        }
                        else {
                            return Object.assign(pair, {
                                x: _cast(pair.x, cast)
                            });
                        }
                    }
                    )
            }
        );

    } else if (field !== undefined && data[field]) {
        return _cast(data[field], cast);

    } else if (_isArray(data)) {
        if (data[0] && data[0][field])
            return data.map(item => _cast(item[field], cast));
        else {
            if (debug)
                console.log("casting gotten array: " + JSON.stringify(data));

            return data.map(item => _cast(item, cast));
        }

    }

    return _cast(data, cast);
}