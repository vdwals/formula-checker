function getAt(input, index, field, cast) {
    const data = get(input, field, cast);

    if (_isArray(data.values)) {
        return Object.assign(data,
            {
                values: [data.values[index]]
            }
        );

    } else if (_isArray(data))
        return data[index];

    return data;
}