/**
 * combines two inputs of different types. First input defines expected result type.
 * 
 * @param a First input, can be a value, array of values, array of objects or an object with array of values. Return type is same as this first input type.
 * @param b Second input, can be a value, array of values, array of objects or an object with array of values.
 * @param fieldA Optional, determine what field to be used of input a
 * @param fieldB Optional, determine what field to be used of input b
 * @param castA Optional, determine what cast function should be applied on field a
 * @param castB Optional, determine what cast function should be applied on field b
 * @returns Result of addition operation.
 */
function plus(a, b, { fieldA, fieldB, fieldResult, castA, castB, castResult } = {}) {
    if (debug) {
        console.log("plus a: " + JSON.stringify(a));
        console.log("plus b: " + JSON.stringify(b));
    }

    const dataA = get(a, fieldA, castA);
    const dataB = get(b, fieldB, castB);

    const firstData = toValueArray(dataA);
    const secondData = toValueArray(dataB);

    if (firstData.length != secondData.length && secondData.length != 1 && firstData.length != 1) {
        console.error("Can not handle different input length: " + firstData.length + " != " + secondData.length);
    }

    const result = [];
    for (let index = 0; index < Math.max(firstData.length, secondData.length); index++) {
        result[index] = firstData[Math.min(index, firstData.length - 1)]
            + secondData[Math.min(index, secondData.length - 1)];
    }

    return _toInputType(result, dataA, fieldResult, castResult);
}