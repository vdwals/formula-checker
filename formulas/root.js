const timeSeries = JSON.parse(timeSeriesData);
const depthSeries = JSON.parse(depthSeriesData);
const eventData = JSON.parse(events);
const parameterData = JSON.parse(parameters);

var debug;
var error;

function _first(...args) {
    for (const value of args) {
        if (value !== undefined && value !== null) return value;
    }
}

function _isFunction(val) {
    return val !== undefined && val !== null && typeof val === 'function';
}

function _fetchData(input) {
    if (debug)
        console.log("fetchData: " + JSON.stringify(input));

    if (input.type) {
        switch (input.type) {
            case "parameter":
                return parameterData
                    .filter((parameter) => parameter.name == input.name)
                    .map(_convertParameter)[0];
            case "timeSeries":
                return timeSeries
                    .filter((time) => time.name == input.name)
                    .map(_convertTimeSeries)[0];
            case "depthSeries":
                return depthSeries
                    .filter((depth) => depth.name == input.name)
                    .map(_convertDepthSeries)[0];
            case "event":
                return eventData
                    .filter((event) => event.name == input.name)
                    .map(_convertEvent);
        }
        return null;
    }

    return input;
}

function _convertParameter(parameter) {
    return Object.assign(parameter,
        {
            // Convert data type
            value: _cast(parameter.value, parameter.type)
        });
}

function _convertTimeSeries(series) {
    if (_isArray(series.values)) {
        const convertedTimestamp = _cast(series.timestamp, "datetime");
        const millisPerStep = series.interval ? series.interval : 1000;

        const convertedValues = series.values.map((pair, index) => {
            return {
                x: _cast(convertedTimestamp.getTime() + (index * millisPerStep), "datetime"),
                y: _cast(pair.y, Number)
            }
        });

        return Object.assign(series,
            {
                timestamp: convertedTimestamp,
                values: convertedValues
            });
    }

    return series;
}

function _convertDepthSeries(series) {
    if (_isArray(series.values)) {
        const convertedValues = series.values.map((pair) => {
            return {
                x: _cast(pair.x, Number),
                y: _cast(pair.y, Number)
            }
        });

        return Object.assign(series,
            {
                values: convertedValues
            });
    }

    return series;
}

function _convertEvent(event) {
    return Object.assign(event, {
        timestamp: _cast(event.timestamp, "datetime")
    });
}

function _cast(value, type) {
    if (type === undefined)
        return value;

    if (typeof type === 'function')
        return type(value);

    switch (type) {
        case "text":
            return "" + value;
        case "number":
            return Number(value);
        case "date":
        case "time":
        case "datetime":
        case "duration":
            if (typeof value === 'string')
                return new Date(Date.parse(value));

            return new Date(value);
        case "boolean":
            return Boolean(value);
        default:
            return value;
    }
}

function toValueArray(input, field, cast) {
    if (debug)
        console.log("InputToArray: " + JSON.stringify(input));

    const data = _fetchData(input);

    let fieldItem = _first(field, "value");

    if (_isArray(data.values)) {
        // Fallback to known value field x
        fieldItem = _first(field,  "x");

        // Fallback to known value type Number
        const castFunction = _first(cast, (item) => Number(item));
        return data.values.map(pair => _cast(pair[fieldItem], castFunction));

    } else if (data[fieldItem])
        return [_cast(input[fieldItem])];

    else if (_isArray(data))
        if (data[0] && data[0][fieldItem])
            return data.map(item => _cast(item[fieldItem], cast));
        else
            return data.map(item => _cast(item, cast));

    else
        return [_cast(data, cast)];
}

function _toInputType(result, input, field, cast) {
    if (debug)
        console.log("toInputType: " + JSON.stringify(result));

    if (input) {
        if (debug)
            console.log("inputType: " + JSON.stringify(input));

        if (_isArray(input.values)) {
            const targetField = _first(field, "x");
            const values = input.values.map(
                (pair, index) => {
                    return Object.assign(pair,
                        { [targetField]: _cast(result[index], cast) }
                    );
                }
            );
            return Object.assign(input, { values: values });

        } else if (input.value) {
            const targetField = _first(field, "value");
            return Object.assign(input, { [targetField]: _cast(result[0], cast) });

        } else if (_isArray(input))
            return result.map(
                (item, index) =>
                    _toInputType([item],
                        input[Math.min(input.length - 1, index)],
                        field,
                        cast)
            );

        else
            return _cast(result[0], cast);
    } else
        return _cast(result, cast);
}

function _isArray(data) {
    return (data !== undefined && data.constructor === Array);
}