function value(input, field, cast) {
    const data = get(input, field, cast);

    if (_isArray(data.values)) {
        return value(data.values, field, cast);

    } else if (_isArray(data) && (data.length == 1)) {
        return value(data[0], field, cast)

    } else if (field !== undefined && data[field] !== undefined)
        return _cast(data[field], cast);

    else if (data.value)
        return value(data, "value");

    else if (data.x)
        return value(data, "x");

    return data;
}