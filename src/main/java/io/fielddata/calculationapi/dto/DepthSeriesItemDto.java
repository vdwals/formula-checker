package io.fielddata.calculationapi.dto;

public class DepthSeriesItemDto {

  // Set to "public" on purpose so that the JS formula can parse the DTO as JSON properly
  public double depth;

  // Set to "public" on purpose so that the JS formula can parse the DTO as JSON properly
  public double value;

  public double getDepth() {
    return this.depth;
  }

  public void setDepth(double depth) {
    this.depth = depth;
  }

  public double getValue() {
    return this.value;
  }

  public void setValue(double value) {
    this.value = value;
  }

}
