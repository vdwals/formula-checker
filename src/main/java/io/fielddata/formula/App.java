package io.fielddata.formula;

import io.fielddata.formula.config.Configs;
import io.fielddata.formula.connection.ConnectionManager;
import io.fielddata.formula.engine.Engine;
import io.fielddata.formula.ui.Ui;
import java.awt.BorderLayout;
import java.util.TimeZone;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

public class App {
  public static void main(String[] args) {
    new Thread(App::new).start();
  }

  public App() {
    TimeZone.setDefault(TimeZone.getTimeZone("UTC"));

    Configs configs = Configs.load();
    ConnectionManager connectionManager = new ConnectionManager();
    Engine engine = new Engine();
    Ui ui = new Ui(configs, connectionManager, engine);

    JFrame frame = new JFrame("Js-Validator");
    frame.setLayout(new BorderLayout(5, 5));

    frame.getContentPane().setLayout(new BorderLayout(5, 5));
    frame.getContentPane().add(ui, BorderLayout.CENTER);

    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    frame.pack();
    frame.setLocationRelativeTo(null);

    SwingUtilities.invokeLater(() -> frame.setVisible(true));
  }
}
