package io.fielddata.formula.config;

import lombok.Data;

@Data
public class Config {
  private String ip;
  private String port = "3306";
  private String username;
  private String password;
  private String database;
}
