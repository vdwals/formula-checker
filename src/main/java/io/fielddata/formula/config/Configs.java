package io.fielddata.formula.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.javalite.common.JsonHelper;

@Data
@Slf4j
public class Configs {
  private Map<String, Config> configs = new HashMap<>();

  @JsonIgnore private static final ObjectMapper mapper = new ObjectMapper();
  @JsonIgnore private static final File configFile = new File("config.json");

  public static Configs load() {
    Configs config = new Configs();

    if (!configFile.exists()) {
      try {
        configFile.createNewFile();
        Files.writeString(configFile.toPath(), JsonHelper.toJsonString(config, true));

      } catch (IOException e) {
        log.error(e.getMessage(), e);
      }
    } else {
      try {
        String configString = Files.readString(configFile.toPath());
        config = mapper.readValue(configString, Configs.class);
      } catch (Exception e) {
        log.error(e.getMessage(), e);
      }
    }
    return config;
  }

  public void save() {
    try {
      Files.writeString(configFile.toPath(), JsonHelper.toJsonString(this, true));
    } catch (IOException e) {
      log.error(e.getMessage(), e);
    }
  }

  public void addConfig(String name, Config config) {
    configs.put(name, config);
    this.save();
  }
}
