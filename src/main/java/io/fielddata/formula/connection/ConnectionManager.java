package io.fielddata.formula.connection;

import io.fielddata.formula.config.Config;
import java.util.HashSet;
import java.util.Set;
import javax.swing.SwingUtilities;
import org.javalite.activejdbc.connection_config.ConnectionJdbcConfig;

public class ConnectionManager {
  private final Set<IConnectionChangeListener> changeListener = new HashSet<>();

  public void open(Config config) {
    ConnectionJdbcConfig connectionConfig =
        new ConnectionJdbcConfig(
            "com.mysql.jdbc.Driver",
            String.format(
                "jdbc:mysql://%s:%s/%s?useSSL=true",
                config.getIp(), config.getPort(), config.getDatabase()),
            config.getUsername(),
            config.getPassword());
    connectionConfig.setEnvironment("development");
    org.javalite.activejdbc.connection_config.DBConfiguration.addConnectionConfig(connectionConfig);

    SwingUtilities.invokeLater(() -> changeListener.forEach(IConnectionChangeListener::connected));
  }

  public void addListener(IConnectionChangeListener listener) {
    this.changeListener.add(listener);
  }
}
