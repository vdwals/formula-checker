package io.fielddata.formula.connection;

public interface IConnectionChangeListener {
  void connected();
}
