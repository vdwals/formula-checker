package io.fielddata.formula.domain;

import java.util.Objects;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
import org.javalite.activejdbc.annotations.BelongsToParents;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

@Table("dataSources")
@IdName("sourceId")
@BelongsToParents({
  @BelongsTo(foreignKeyName = "elementId", parent = Element.class),
  @BelongsTo(foreignKeyName = "techniqueId", parent = Technique.class)
})
public class DataSource extends Model {

  public int getTechniqueId() {
    return getInteger("techniqueId");
  }

  @Override
  public String toString() {
    return Objects.requireNonNullElse(this.getString("operator"), "")
        + " "
        + getTimestamp("starttime")
        + " "
        + getInteger("techniqueId");
  }

  public Long getElementId() {
    return getLong("elementId");
  }
}
