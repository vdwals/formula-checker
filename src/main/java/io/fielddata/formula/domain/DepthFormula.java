package io.fielddata.formula.domain;

import java.util.Collection;
import java.util.List;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;
import org.javalite.common.Util;

@Table("depthSeriesFormula")
@IdName("depthSeriesTypeId")
@BelongsTo(foreignKeyName = "depthSeriesTypeId", parent = DepthType.class)
public class DepthFormula extends Model {

  public static final String OEM_ID = "oemId";
  public static final String DEPENDENCIES = "dependencies";
  public static final String FORMULA = "formula";

  public String getFormula() {
    return getString(FORMULA);
  }

  public String getDependencies() {
    return getString(DEPENDENCIES);
  }

  public static LazyList<DepthFormula> findByTypeIds(Collection<Long> typeIds) {
    if (typeIds == null || typeIds.isEmpty()) {
      return DepthFormula.findAll();
    }
    return DepthFormula
        .where(
            String
                .format(
                    "%s in (%s)",
                    DepthFormula.getMetaModel().getIdName(),
                    Util.join(typeIds, ",")));
  }

  public Long getOemId() {
    return getLong(OEM_ID);
  }

  public static LazyList<DepthType> findByTypeIdsAndOemId(List<Long> typeIds, Long selectedOemId) {
    if (typeIds == null || typeIds.isEmpty()) {
      return DepthFormula.findAll();
    }
    return DepthFormula
        .where(
            String
                .format(
                    "%s in (%s) and %s = %s",
                    DepthFormula.getMetaModel().getIdName(),
                    Util.join(typeIds, ","),
                    OEM_ID,
                    selectedOemId));
  }
}
