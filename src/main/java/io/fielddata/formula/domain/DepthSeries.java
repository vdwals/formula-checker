package io.fielddata.formula.domain;

import java.util.List;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
import org.javalite.activejdbc.annotations.BelongsToParents;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;
import org.javalite.common.Util;

@Table("depthSeriesData")
@IdName("measurementId")
@BelongsToParents({@BelongsTo(foreignKeyName = "depthTypeId", parent = DepthType.class),
    @BelongsTo(foreignKeyName = "sourceId", parent = DataSource.class)})
public class DepthSeries extends Model {

  @SuppressWarnings("unchecked")
  public static LazyList<DepthSeries> findBySourceIdAndTypeIds(int sourceId, List<Long> typeIds) {
    return DepthSeries
        .where(String.format("sourceId = ? and typeId in (%s)", Util.join(typeIds, ",")), sourceId)
        .include(DepthSeriesData.class);
  }

  public static List<DepthSeries> findBySourceId(Integer sourceId) {
    return DepthSeries.where("sourceId = ?", sourceId);
  }
}
