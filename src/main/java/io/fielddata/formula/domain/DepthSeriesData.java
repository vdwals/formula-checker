package io.fielddata.formula.domain;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
import org.javalite.activejdbc.annotations.Table;

@Table("depthSeriesDataDetails")
@BelongsTo(foreignKeyName = "measurementId", parent = DepthSeries.class)
public class DepthSeriesData extends Model {

  private static final String DEPTH = "depth";
  private static final String VALUE = "value";

  public String getValue() {
    return getString(VALUE);
  }

  public String getDepth() {
    return getString(DEPTH);
  }
}
