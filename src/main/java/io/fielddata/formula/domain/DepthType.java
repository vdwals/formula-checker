package io.fielddata.formula.domain;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
import org.javalite.activejdbc.annotations.Cached;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

@Table("depthSeriesTypes")
@IdName("typeId")
@BelongsTo(foreignKeyName = "techniqueId", parent = Technique.class)
@Cached
public class DepthType extends Model {
  public static final String TYPE_NAME = "typeName";

  @Override
  public String toString() {
    return getString(TYPE_NAME);
  }

}
