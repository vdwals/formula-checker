package io.fielddata.formula.domain;

import java.util.List;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.IdName;

@IdName("elementId")
public class Element extends Model {

  public static List<Element> all() {
    return Element.findAll();
  }

  public static List<Element> findByProjectId(int projectId) {
    return Element.where("projectId = ?", projectId);
  }

  public Long getProjectId() {
    return getLong("projectId");
  }

  @Override
  public String toString() {
    return this.getLongId() + " - " + this.getString("elementName");
  }
}
