package io.fielddata.formula.domain;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

@Table("event")
@IdName("eventId")
public class Event extends Model {

  private static final String EVENT_NAME = "eventName";

  public String getName() {
    return getString(EVENT_NAME);
  }

}
