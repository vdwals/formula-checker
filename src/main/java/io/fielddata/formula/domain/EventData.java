package io.fielddata.formula.domain;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
import org.javalite.activejdbc.annotations.BelongsToParents;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;
import org.javalite.common.Util;

@Table("eventData")
@BelongsToParents({@BelongsTo(foreignKeyName = "sourceId", parent = DataSource.class),
    @BelongsTo(foreignKeyName = "eventId", parent = Event.class)})
@IdName("instanceId")
public class EventData extends Model {

  private static final String VALUE = "value";
  private static final String TIMESTAMP = "timestamp";

  public static LazyList<EventData> findBySourceIdAndEventIds(int sourceId, List<Long> eventIds) {
    return EventData
        .where(
            String.format("sourceId = ? and eventId in (%s)", Util.join(eventIds, ",")),
            sourceId);
  }

  public LocalDateTime getTimestamp() {
    Timestamp timestamp = getTimestamp(TIMESTAMP);
    if (timestamp == null) {
      return null;
    }

    return timestamp.toLocalDateTime();
  }

  public String getValue() {
    return getString(VALUE);
  }
}
