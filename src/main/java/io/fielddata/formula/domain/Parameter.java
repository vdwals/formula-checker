package io.fielddata.formula.domain;

import java.util.List;
import java.util.Objects;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
import org.javalite.activejdbc.annotations.Cached;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

@IdName("paramId")
@Table("parameters")
@BelongsTo(foreignKeyName = "techniqueId", parent = Technique.class)
@Cached
public class Parameter extends Model {
  public static final String TEMPLATE_KEY = "templateKey";

  public String getTemplateKey() {
    return getString(TEMPLATE_KEY);
  }

  public static List<Parameter> findByTechniqueId(int techniqueId) {
    return Parameter.where("techniqueId = ? and (valid = 1 or projectId is not null)", techniqueId);
  }

  @Override
  public String toString() {
    return getString("paramName") + " " + Objects.requireNonNullElse(getString("templateKey"), "");
  }
}
