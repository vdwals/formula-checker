package io.fielddata.formula.domain;

import java.util.List;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
import org.javalite.activejdbc.annotations.BelongsToParents;
import org.javalite.activejdbc.annotations.CompositePK;
import org.javalite.activejdbc.annotations.Table;
import org.javalite.common.Util;

@CompositePK({"paramId", "elementId"})
@Table("parameterDataActual")
@BelongsToParents({@BelongsTo(foreignKeyName = "paramId", parent = Parameter.class),
    @BelongsTo(foreignKeyName = "elementId", parent = Element.class)})
public class ParameterData extends Model {

  private static final String COLUMN_VALUE = "value";

  public static LazyList<ParameterData> findByElementIdAndParamIds(
      int elementId,
      List<Long> paramIds) {
    return ParameterData
        .where(
            String.format("elementId = ? and paramId in (%s)", Util.join(paramIds, ",")),
            elementId);
  }

  public String getValue() {
    return getString(COLUMN_VALUE);
  }
}
