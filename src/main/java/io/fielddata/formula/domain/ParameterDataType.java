package io.fielddata.formula.domain;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Cached;
import org.javalite.activejdbc.annotations.HasMany;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

@IdName("typeId")
@Table("parameterDataType")
@HasMany(child = Parameter.class, foreignKeyName = "dataTypeId")
@Cached
public class ParameterDataType extends Model {

  private static final String TYPE_NAME = "typeName";

  public String getType() {
    return getString(TYPE_NAME);
  }
}
