package io.fielddata.formula.domain;

import java.util.Collection;
import java.util.List;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;
import org.javalite.common.Util;

@IdName("paramId")
@Table("parameterFormula")
@BelongsTo(foreignKeyName = "paramId", parent = Parameter.class)
public class ParameterFormula extends Model {

  public static final String OEM_ID = "oemId";
  public static final String DEPENDENCIES = "sourceOfData";
  public static final String FORMULA = "formula";

  public static LazyList<ParameterFormula> findByParamIds(Collection<Long> parameterIds) {
    return ParameterFormula.where(
        String.format(
            "%s in (%s)",
            ParameterFormula.getMetaModel().getIdName(), Util.join(parameterIds, ",")));
  }

  public String getFormula() {
    return getString(FORMULA);
  }

  public String getDependencies() {
    return getString(DEPENDENCIES);
  }

  public Long getOemId() {
    return getLong(OEM_ID);
  }

  public static LazyList<Parameter> findByParamIdsAndOemId(
      List<Long> parameterIds,
      Long selectedOemId) {
    return ParameterFormula
        .where(
            String
                .format(
                    "%s in (%s) and %s = %s",
                    ParameterFormula.getMetaModel().getIdName(),
                    Util.join(parameterIds, ","),
                    OEM_ID,
                    selectedOemId));
  }
}
