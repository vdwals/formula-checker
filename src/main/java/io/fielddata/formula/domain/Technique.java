package io.fielddata.formula.domain;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Cached;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;

@Table("techniques")
@IdName("techniqueId")
@Cached
public class Technique extends Model {
  private static final String TECHNIQUE_NAME = "techniqueName";

  @Override
  public String toString() {
    return String.format("%s - %s", getLongId(), getString(TECHNIQUE_NAME));
  }

  public String getName() {
    return getString(TECHNIQUE_NAME);
  }
}
