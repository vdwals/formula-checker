package io.fielddata.formula.domain;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.BelongsTo;
import org.javalite.activejdbc.annotations.BelongsToParents;
import org.javalite.activejdbc.annotations.IdName;
import org.javalite.activejdbc.annotations.Table;
import org.javalite.common.Util;

@Table("timeSeriesData")
@IdName("typeId")
@BelongsToParents({@BelongsTo(foreignKeyName = "typeId", parent = TimeType.class),
    @BelongsTo(foreignKeyName = "sourceId", parent = DataSource.class)})
public class TimeSeriesData extends Model {

  private static final String REGEX = ",";
  private static final String DATA = "data";

  public static LazyList<TimeSeriesData> findBySourceIdAndTypeIds(
      int sourceId,
      List<Long> typeIds) {
    return TimeSeriesData
        .where(
            String.format("sourceId = ? and typeId in (%s)", Util.join(typeIds, REGEX)),
            sourceId);
  }

  public List<String> getData() {
    return Arrays.asList(this.getString(DATA).split(REGEX));
  }

  public LocalDateTime getTimestamp() {
    Timestamp timestamp = getTimestamp("timestamp");
    if (timestamp == null) {
      return null;
    }

    return timestamp.toLocalDateTime();
  }
}
