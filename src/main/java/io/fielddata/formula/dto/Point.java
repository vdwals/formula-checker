package io.fielddata.formula.dto;

import lombok.Value;

@Value
public class Point {
  String x, y;
}
