package io.fielddata.formula.dto;

import java.time.LocalDateTime;
import lombok.Value;

@Value
public class ValueObjectDto {
  LocalDateTime timestamp;
  String value;
  String name;
  String dataType;
}
