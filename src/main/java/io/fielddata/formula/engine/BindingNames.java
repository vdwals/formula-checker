package io.fielddata.formula.engine;

public final class BindingNames {

  public static final String TIME_SERIES_DATA = "timeSeriesData";

  public static final String DEPTH_SERIES_DATA = "depthSeriesData";

  public static final String EVENT_DATA = "events";

  public static final String DEPENDENT_PARAM = "parameters";

  private BindingNames() {
  }
}
