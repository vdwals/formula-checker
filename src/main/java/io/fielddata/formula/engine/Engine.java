package io.fielddata.formula.engine;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import io.fielddata.formula.domain.DataSource;
import io.fielddata.formula.domain.DepthSeries;
import io.fielddata.formula.domain.DepthSeriesData;
import io.fielddata.formula.domain.DepthType;
import io.fielddata.formula.domain.Element;
import io.fielddata.formula.domain.Event;
import io.fielddata.formula.domain.EventData;
import io.fielddata.formula.domain.Parameter;
import io.fielddata.formula.domain.ParameterData;
import io.fielddata.formula.domain.ParameterDataType;
import io.fielddata.formula.domain.TimeSeriesData;
import io.fielddata.formula.domain.TimeType;
import io.fielddata.formula.dto.Point;
import io.fielddata.formula.dto.SeriesObjectDto;
import io.fielddata.formula.dto.ValueObjectDto;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.SwingUtilities;
import lombok.extern.slf4j.Slf4j;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.HostAccess;
import org.graalvm.polyglot.Value;
import org.javalite.activejdbc.Base;

@Slf4j
public class Engine {
  private final ObjectMapper mapper = new ObjectMapper()
      .findAndRegisterModules()
      .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

  public String evaluateFormula(
      String formula,
      RSyntaxTextArea fullFormulaField,
      int elementId,
      int sourceId) {

    String contextLanguage = "js";
    Context context = Context
        .newBuilder(contextLanguage)
        .allowHostAccess(HostAccess.ALL)
        .allowAllAccess(true)
        .allowHostClassLookup(className -> true)
        .build();

    Instant start = Instant.now();

    Value bindings = context.getBindings(contextLanguage);
    bindValues(elementId, sourceId).forEach(bindings::putMember);

    Instant bound = Instant.now();

    StringBuilder sb = new StringBuilder();

    try (Stream<Path> paths = Files.walk(Paths.get("formulas"))) {

      paths.filter(Files::isRegularFile).map(t -> {
        try {
          return Files.readAllBytes(t);
        } catch (IOException e) {
          log.error(e.getMessage(), e);
          return null;
        }
      })
          .filter(Objects::nonNull)
          .map(bytes -> new String(bytes, StandardCharsets.UTF_8))
          .forEach(file -> {
            sb.append(file);
            sb.append(System.lineSeparator());
          });

    } catch (Exception e1) {
      log.error(e1.getMessage(), e1);
    }

    sb.append(String.format("result = JSON.stringify(%s);", formula));
    sb.append(System.lineSeparator());

    String fullFormula = sb.toString();
    SwingUtilities.invokeLater(() -> {
      fullFormulaField.setText(fullFormula);
    });

    Instant formalBuild = Instant.now();

    try {
      context.eval(contextLanguage, fullFormula);

      Instant evaluated = Instant.now();

      Value result = bindings.getMember("result");

      String prettyResult = result.toString();
      // if (!result.isMetaObject()) {
      // prettyResult = result.toString();
      // } else {
      // prettyResult = JsonHelper.toJsonString(JsonHelper.toMap(result.toString()), true);
      // }

      log.info("Binding duration; {}", humanReadableFormat(Duration.between(start, bound)));
      log
          .info(
              "Formal building duration; {}",
              humanReadableFormat(Duration.between(bound, formalBuild)));
      log
          .info(
              "Evaluation duration; {}",
              humanReadableFormat(Duration.between(formalBuild, evaluated)));
      log.info("Total duration; {}", humanReadableFormat(Duration.between(start, evaluated)));

      return prettyResult;
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return e.getMessage();
    }
  }

  public String humanReadableFormat(Duration duration) {
    return duration.toString().substring(2).replaceAll("(\\d[HMS])(?!$)", "$1 ").toLowerCase();
  }

  private Map<String, Object> bindValues(int elementId, int sourceId) {
    return Base.withDb(() -> {
      Map<String, Object> bindingValues = new HashMap<>();
      try {
        Element element = Element.findById(elementId);

        bindObjects(
            bindingValues,
            getParameters(element),
            BindingNames.DEPENDENT_PARAM,
            "Parameters");


        DataSource dataSource = DataSource.findById(sourceId);

        bindObjects(bindingValues, getEvents(dataSource), BindingNames.EVENT_DATA, "Events");

        bindObjects(
            bindingValues,
            getDepthSeries(dataSource),
            BindingNames.DEPTH_SERIES_DATA,
            "DepthData");

        bindObjects(
            bindingValues,
            getTimeSeries(dataSource),
            BindingNames.TIME_SERIES_DATA,
            "TimeSeries");
      } catch (JsonProcessingException e) {
        log.error(e.getMessage(), e);
      }

      return bindingValues;
    });
  }

  private List<Object> getTimeSeries(DataSource dataSource) {
    return dataSource.getAll(TimeSeriesData.class).stream().map(data -> {
      TimeType seriesType = data.parent(TimeType.class);

      List<Point> values =
          data.getData().stream().map(value -> new Point(value, null)).collect(Collectors.toList());

      return new SeriesObjectDto(data.getTimestamp(), values, seriesType.toString());
    }).collect(Collectors.toList());
  }

  private List<Object> getDepthSeries(DataSource dataSource) {
    return dataSource.getAll(DepthSeries.class).stream().map(data -> {
      DepthType seriesType = data.parent(DepthType.class);

      List<Point> values = data
          .getAll(DepthSeriesData.class)
          .stream()
          .map(detail -> new Point(detail.getValue(), detail.getDepth()))
          .collect(Collectors.toList());

      return new SeriesObjectDto(null, values, seriesType.toString());
    }).collect(Collectors.toList());
  }

  private List<Object> getEvents(DataSource dataSource) {
    return dataSource.getAll(EventData.class).stream().map(data -> {
      Event event = data.parent(Event.class);
      return new ValueObjectDto(data.getTimestamp(), data.getValue(), event.getName(), "text");
    }).collect(Collectors.toList());
  }

  private void bindObjects(
      Map<String, Object> bindingValues,
      List<Object> data,
      String key,
      String description)
      throws JsonProcessingException {

    String jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);

    bindingValues.put(key, jsonString);
    log.debug("{} added: {}", description, jsonString);
  }

  private List<Object> getParameters(Element element) {
    return element.getAll(ParameterData.class).stream().map(data -> {
      Parameter parameter = data.parent(Parameter.class);

      String name = parameter.getTemplateKey();
      String value = data.getValue();

      ParameterDataType dataType = parameter.parent(ParameterDataType.class);
      return new ValueObjectDto(null, value, name, dataType.getType());
    }).collect(Collectors.toList());
  }
}
