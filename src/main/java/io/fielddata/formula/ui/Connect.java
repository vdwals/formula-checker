package io.fielddata.formula.ui;

import io.fielddata.formula.config.Config;
import io.fielddata.formula.config.Configs;
import io.fielddata.formula.connection.ConnectionManager;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import lombok.Getter;

public class Connect extends JPanel {

  private static final long serialVersionUID = -7080095691724273474L;

  private final JTextField host, port, username, password, database;
  private final JComboBox<String> configSelector;
  private final JButton add, delete;

  private JButton connect;

  @Getter private Config config;

  public Connect(Configs configs, ConnectionManager connectionManager) {

    host = new JTextField();
    port = new JTextField();
    database = new JTextField();
    username = new JTextField();
    password = new JPasswordField();
    add = new JButton("add");
    delete = new JButton("delete");

    configSelector = new JComboBox<>(new Vector<>(configs.getConfigs().keySet()));

    connect = new JButton("Connect");

    add.addActionListener(
        a -> {
          String newConfigName = JOptionPane.showInputDialog("Enter config name: ");

          if (newConfigName != null) {
            configs.addConfig(newConfigName, new Config());

            configSelector.addItem(newConfigName);
            configSelector.setSelectedItem(newConfigName);
          }
        });

    delete.addActionListener(
        a -> {
          String configName = (String) configSelector.getSelectedItem();

          if (configName != null) {
            configs.getConfigs().remove(configName);
            configs.save();
            configSelector.removeItem(configName);
          }
        });

    configSelector.addActionListener(
        a -> {
          String configName = (String) configSelector.getSelectedItem();

          if (configName != null) {
            config = configs.getConfigs().get(configName);

            host.setText(config.getIp());
            port.setText(config.getPort());
            database.setText(config.getDatabase());
            username.setText(config.getUsername());
            password.setText(config.getPassword());
          }
        });

    connect.addActionListener(
        a -> {
          String configName = (String) configSelector.getSelectedItem();

          if (configName != null) {
            config = configs.getConfigs().get(configName);

            config.setIp(host.getText());
            config.setPort(port.getText());
            config.setUsername(username.getText());
            config.setPassword(password.getText());
            config.setDatabase(database.getText());
            configs.save();

            new Thread(() -> connectionManager.open(config)).start();
          }
        });

    build();
  }

  private void build() {
    removeAll();

    setLayout(new BorderLayout(5, 5));
    JPanel top = new JPanel(new GridLayout(2, 1));
    JPanel buttons = new JPanel(new GridLayout(1, 2));

    buttons.add(add);
    buttons.add(delete);

    top.add(configSelector);
    top.add(buttons);

    this.add(top, BorderLayout.NORTH);

    JPanel labels = new JPanel(new GridLayout(6, 1));
    labels.add(new JLabel("Host:"));
    labels.add(new JLabel("Port:"));
    labels.add(new JLabel("Database:"));
    labels.add(new JLabel("Username:"));
    labels.add(new JLabel("Password:"));
    labels.add(connect);

    this.add(labels, BorderLayout.WEST);

    JPanel fields = new JPanel(new GridLayout(6, 1));
    fields.add(host);
    fields.add(port);
    fields.add(database);
    fields.add(username);
    fields.add(password);
    fields.add(new JLabel());

    this.add(fields, BorderLayout.CENTER);

    if (configSelector.getItemCount() > 0) {
      configSelector.setSelectedIndex(0);
    }

    validate();
    repaint();
  }
}
