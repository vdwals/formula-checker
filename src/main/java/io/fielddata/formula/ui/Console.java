package io.fielddata.formula.ui;

import io.fielddata.formula.ui.helper.MessageConsole;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

public class Console extends JPanel {

  private static final long serialVersionUID = -9044913762242705947L;
  private final JTextPane output;

  public Console() {
    output = new JTextPane();

    build();
  }

  private void build() {
    this.setLayout(new BorderLayout(5, 5));

    JScrollPane editorScrollPane = new JScrollPane(output);
    editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    editorScrollPane.setPreferredSize(new Dimension(250, 145));
    editorScrollPane.setMinimumSize(new Dimension(10, 10));

    this.add(editorScrollPane, BorderLayout.CENTER);

    MessageConsole mc = new MessageConsole(output);
    mc.redirectOut();
    mc.redirectErr(Color.RED, null);
    mc.setMessageLines(1000);

    this.validate();
    this.repaint();
  }
}
