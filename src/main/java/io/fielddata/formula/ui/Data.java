package io.fielddata.formula.ui;

import io.fielddata.formula.connection.ConnectionManager;
import io.fielddata.formula.domain.DataSource;
import io.fielddata.formula.domain.Element;
import io.fielddata.formula.domain.Technique;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import org.javalite.activejdbc.Base;

public class Data extends JPanel implements IDataProvider, IDataChangeListener {

  private static final long serialVersionUID = 7727665991551660705L;

  private final JComboBox<Long> projectIdSelector;
  private final JComboBox<Element> elementSelector;
  private final JComboBox<DataSource> datasourceSelector;
  private final JProgressBar progressBar;

  private List<DataSource> dataSources;

  private long dataSourceId = 0;

  public Data(ConnectionManager connectionManager) {

    projectIdSelector = new JComboBox<>();
    elementSelector = new JComboBox<>();
    datasourceSelector = new JComboBox<>();
    progressBar = new JProgressBar();

    projectIdSelector.setEditable(false);
    elementSelector.setEditable(false);
    datasourceSelector.setEditable(false);

    projectIdSelector.addActionListener(
        a -> {
          Long projectId = (Long) projectIdSelector.getSelectedItem();

          progressBar.setIndeterminate(true);
          elementSelector.setEditable(false);
          elementSelector.removeAllItems();

          if (projectId != null) {
            new Thread(
                    () ->
                        Base.withDb(
                            () -> {
                              SwingUtilities.invokeLater(
                                  () -> {
                                    dataSources
                                        .stream()
                                        .map(source -> source.parent(Element.class))
                                        .filter(element -> projectId.equals(element.getProjectId()))
                                        .sorted(Comparator.comparing(Element::toString))
                                        .forEach(element -> elementSelector.addItem(element));

                                    if (elementSelector.getItemCount() > 0) {
                                      progressBar.setIndeterminate(true);
                                      elementSelector.setSelectedIndex(0);
                                      elementSelector.setEditable(false);
                                    } else {
                                      progressBar.setIndeterminate(false);
                                    }
                                  });

                              return null;
                            }))
                .start();
          }
        });

    elementSelector.addActionListener(
        a -> {
          Element selectedItem = (Element) elementSelector.getSelectedItem();
          datasourceSelector.removeAllItems();

          if (selectedItem != null) {
            Long elementId = selectedItem.getLongId();
            new Thread(
                    () ->
                        Base.withDb(
                            () -> {
                              SwingUtilities.invokeLater(
                                  () -> {
                                    dataSources
                                        .stream()
                                        .filter(
                                            dataSource ->
                                                dataSource.getElementId().equals(elementId))
                                        .sorted(Comparator.comparing(DataSource::toString))
                                        .forEach(datasourceSelector::addItem);

                                    if (datasourceSelector.getItemCount() > 0) {
                                      progressBar.setIndeterminate(true);
                                      datasourceSelector.setSelectedIndex(0);
                                      datasourceSelector.setEnabled(true);
                                    } else {
                                      progressBar.setIndeterminate(false);
                                    }
                                  });
                              return null;
                            }))
                .start();
          }
        });

    datasourceSelector.addActionListener(
        a -> {
          DataSource ds = (DataSource) datasourceSelector.getSelectedItem();
          if (ds != null) {
            if (!ds.getLongId().equals(dataSourceId)) {
              System.out.println("Datasource selected: " + ds.getLongId());
              dataSourceId = ds.getLongId();
            }
            progressBar.setIndeterminate(false);
          }
        });

    build();
  }

  private void build() {
    removeAll();

    setLayout(new BorderLayout(5, 5));
    JPanel labels = new JPanel(new GridLayout(5, 1));
    labels.add(new JLabel("Project Id:"));
    labels.add(new JLabel("Element:"));
    labels.add(new JLabel("Datasource:"));

    this.add(labels, BorderLayout.WEST);

    JPanel fields = new JPanel(new GridLayout(5, 1));
    fields.add(projectIdSelector);
    fields.add(elementSelector);
    fields.add(datasourceSelector);

    this.add(fields, BorderLayout.CENTER);
    this.add(progressBar, BorderLayout.SOUTH);

    validate();
    repaint();
  }

  @Override
  public Integer getElementId() {
    return Base.withDb(
        () -> {
          Element selectedItem = (Element) elementSelector.getSelectedItem();
          if (selectedItem != null) {
            return selectedItem.getLongId().intValue();
          }
          return null;
        });
  }

  @Override
  public Integer getTechniqueId() {
    return Base.withDb(
        () -> {
          Object selectedItem = datasourceSelector.getSelectedItem();
          if (selectedItem != null) {
            return ((DataSource) selectedItem).getTechniqueId();
          }
          return null;
        });
  }

  @Override
  public Integer getSourceId() {
    Object selectedItem = datasourceSelector.getSelectedItem();
    if (selectedItem == null) {
      return null;
    }
    return Base.withDb(() -> ((DataSource) selectedItem).getLongId().intValue());
  }

  @SuppressWarnings("unchecked")
  @Override
  public void dataChanged(Technique technique) {
    progressBar.setIndeterminate(true);

    SwingUtilities.invokeLater(
        () -> {
          dataSources =
              Base.withDb(() -> technique.getAll(DataSource.class).include(Element.class));

          Set<Long> projectIds =
              Base.withDb(
                  () ->
                      dataSources
                          .stream()
                          .map(dataSource -> dataSource.parent(Element.class))
                          .map(Element::getProjectId)
                          .collect(Collectors.toSet()));

          Long currentProjectId = (Long) projectIdSelector.getSelectedItem();

          projectIdSelector.removeAllItems();

          projectIds.stream().sorted().forEach(id -> projectIdSelector.addItem(id));

          if (projectIdSelector.getItemCount() > 0
              && (currentProjectId == null || !projectIds.contains(currentProjectId))) {
            projectIdSelector.setSelectedIndex(0);
          } else {
            progressBar.setIndeterminate(false);
          }
        });
  }
}
