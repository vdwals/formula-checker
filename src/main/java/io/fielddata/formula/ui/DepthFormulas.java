package io.fielddata.formula.ui;

import io.fielddata.formula.connection.ConnectionManager;
import io.fielddata.formula.domain.DepthFormula;
import io.fielddata.formula.domain.DepthType;
import io.fielddata.formula.domain.Technique;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.ObjectUtils;
import org.javalite.activejdbc.Base;
import org.javalite.common.JsonHelper;

public class DepthFormulas extends Formulas<DepthType> {

  private static final long serialVersionUID = 7727665991551660705L;

  public DepthFormulas(ConnectionManager connectionManager, Connect connect) {
    super(connectionManager, connect);
  }

  @Override
  protected Object filterOems() {
    Technique selected = (Technique) getTechniqueSelector().getSelectedItem();
    getOemSelector().removeAllItems();

    if (selected != null) {
      List<Long> ids = selected
          .getAll(DepthType.class)
          .stream()
          .map(DepthType::getLongId)
          .collect(Collectors.toList());

      DepthFormula
          .findByTypeIds(ids)
          .stream()
          .map(DepthFormula::getOemId)
          .collect(Collectors.toSet())
          .forEach(oemId -> getOemSelector().addItem(oemId));

    }
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  protected Object filterFormulas() {
    Technique selectedTechnique = (Technique) getTechniqueSelector().getSelectedItem();
    Long selectedOemId = (Long) getOemSelector().getSelectedItem();
    getItemSelector().removeAllItems();

    if (selectedTechnique != null && selectedOemId != null) {
      List<DepthType> depthTypes = selectedTechnique.getAll(DepthType.class);

      if (getFilterCheckbox().isSelected()) {
        List<Long> ids = depthTypes.stream().map(DepthType::getLongId).collect(Collectors.toList());
        depthTypes = DepthFormula
            .findByTypeIdsAndOemId(ids, selectedOemId)
            .include(DepthType.class)
            .stream()
            .map(depthFormula -> ((DepthFormula) depthFormula).parent(DepthType.class))
            .collect(Collectors.toList());
      }

      depthTypes
          .stream()
          .sorted(Comparator.comparing(DepthType::toString))
          .forEach(depthType -> getItemSelector().addItem(depthType));
    }
    return null;
  }

  @Override
  protected Object loadFromDb(long id) {
    DepthFormula formula = DepthFormula.findById(id);

    String jsScript = formula.getFormula();

    String dependencies = formula.getDependencies();

    if (jsScript != null) {
      getJsCodeEditor().setText(jsScript);
    }
    if (dependencies != null) {
      dependencies = JsonHelper.toJsonString(JsonHelper.toMap(dependencies), true);

      getDependenciesField().setText(dependencies);
    }
    return null;
  }

  @Override
  protected String getItemName() {
    return "Depth";
  }

  @Override
  protected void saveUpdateSql() {
    DepthType selectedItem = (DepthType) getItemSelector().getSelectedItem();
    Technique selectedTechnique = (Technique) getTechniqueSelector().getSelectedItem();

    if (ObjectUtils.allNotNull(selectedItem, selectedTechnique)) {
      String update = MessageFormat
          .format(
              UPDATE_SQL,
              new Date(),
              connect.getConfig().getDatabase(),
              DepthFormula.getTableName(),
              DepthType.getTableName(),
              selectedItem.getIdName(),
              DepthFormula.metaModel().getIdName(),
              DepthType.TYPE_NAME,
              selectedItem.toString(),
              selectedTechnique.getName(),
              DepthFormula.FORMULA,
              getJsCodeEditor().getText(),
              DepthFormula.DEPENDENCIES,
              getDependenciesField().getText(),
              DepthFormula.OEM_ID,
              getOemSelector().getSelectedItem());
      saveToFile(update);
    } else {
      System.err.println("No parameter selected.");
    }
  }

  @Override
  protected void saveInsertSql() {
    DepthType selectedItem = (DepthType) getItemSelector().getSelectedItem();
    Technique selectedTechnique = (Technique) getTechniqueSelector().getSelectedItem();

    if (ObjectUtils.allNotNull(selectedItem, selectedTechnique)) {
      String insert = MessageFormat
          .format(
              INSERT_SQL,
              new Date(),
              connect.getConfig().getDatabase(),
              DepthFormula.getTableName(),
              DepthFormula.metaModel().getIdName(),
              DepthFormula.OEM_ID,
              DepthFormula.FORMULA,
              DepthFormula.DEPENDENCIES,
              selectedItem.getIdName(),
              getOemSelector().getSelectedItem(),
              getJsCodeEditor().getText(),
              getDependenciesField().getText(),
              DepthType.getTableName(),
              selectedTechnique.getName(),
              DepthType.TYPE_NAME,
              selectedItem.toString());

      saveToFile(insert);
    } else {
      System.err.println("No parameter selected.");
    }
  }

  @Override
  protected void addOems() {
    Base.withDb(() -> {
      DepthFormula
          .findAll()
          .stream()
          .map(depthFormula -> (DepthFormula) depthFormula)
          .map(DepthFormula::getOemId)
          .collect(Collectors.toSet())
          .forEach(oemId -> getOemSelector().addItem(oemId));

      if (getOemSelector().getItemCount() > 0) {
        getOemSelector().setSelectedIndex(0);
      }
      return null;
    });
  }

  @Override
  protected void customBuild() {}

  @Override
  public boolean asMap() {
    return true;
  }
}
