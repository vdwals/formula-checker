package io.fielddata.formula.ui;

import io.fielddata.formula.engine.Engine;
import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import org.apache.commons.lang3.ObjectUtils;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;

public class Eval extends JPanel {

  private static final long serialVersionUID = 5298661326544136782L;

  private final RSyntaxTextArea resultText;
  private final JButton evaluate;
  private final JProgressBar progressBar;

  public Eval(IFormulaProvider formulaProvider, IDataProvider dataProvider, Engine engine) {
    resultText = new RSyntaxTextArea();
    resultText.setEditable(false);
    resultText.setTabSize(3);
    resultText.setCaretPosition(0);
    resultText.requestFocusInWindow();
    resultText.setMarkOccurrences(true);
    resultText.setCodeFoldingEnabled(true);
    resultText.setClearWhitespaceLinesEnabled(false);
    resultText.setHighlightCurrentLine(false);
    if (formulaProvider.asMap()) {
      resultText.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JSON);
      resultText.setRows(20);
    } else {
      resultText.setRows(5);
    }

    progressBar = new JProgressBar();

    evaluate = new JButton("Evaluate");
    evaluate.addActionListener(a -> {
      if (ObjectUtils.allNotNull(dataProvider.getElementId(), dataProvider.getSourceId())) {
        progressBar.setIndeterminate(true);
        evaluate.setEnabled(false);

        new Thread(() -> {
          String evaluateFormula = engine
              .evaluateFormula(
                  formulaProvider.getJsFormula(),
                  formulaProvider.getFullFormulaField(),
                  dataProvider.getElementId(),
                  dataProvider.getSourceId());

          SwingUtilities.invokeLater(() -> {
            resultText.setText(evaluateFormula);
            progressBar.setIndeterminate(false);
            evaluate.setEnabled(true);
          });
        }).start();
      }
    });

    build();
  }

  private void build() {
    removeAll();

    setLayout(new BorderLayout(5, 5));

    this.add(evaluate, BorderLayout.NORTH);
    this.add(progressBar, BorderLayout.SOUTH);

    RTextScrollPane sp = new RTextScrollPane(resultText);
    this.add(sp, BorderLayout.CENTER);

    validate();
    repaint();
  }
}
