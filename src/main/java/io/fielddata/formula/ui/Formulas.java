package io.fielddata.formula.ui;

import io.fielddata.formula.connection.ConnectionManager;
import io.fielddata.formula.connection.IConnectionChangeListener;
import io.fielddata.formula.domain.Technique;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.Model;

@Slf4j
public abstract class Formulas<T extends Model> extends JPanel
    implements IConnectionChangeListener, IFormulaProvider {

  protected static final String UPDATE_SQL =
      "--  Auto-generated SQL script #{0}\r\n"
          + "UPDATE {1}.{2} a\r\n"
          + "   inner join  {1}.{3} b \r\n"
          + "      on  b.{4} = a.{5} \r\n"
          + "      and b.{6} = ''{7}''\r\n"
          + "   inner JOIN  {1}.techniques t\r\n"
          + "      on  t.techniqueId = b.techniqueId \r\n"
          + "      and t.techniqueName = ''{8}''\r\n"
          + "    SET {9}=''{10}'',{11}=''{12}''\r\n"
          + "   where a.{13} = {14}\r\n;";

  protected static final String INSERT_SQL =
      "--  Auto-generated SQL script #{0}\r\n"
          + "INSERT INTO {1}.{2} ({3},{4},{5},{6})\r\n"
          + "    SELECT     b.{7},\r\n"
          + "               {8},\r\n"
          + "               ''{9}'',\r\n"
          + "               ''{10}''\r\n"
          + "    FROM    {1}.{11} b \r\n"
          + "        inner JOIN  {1}.techniques t\r\n"
          + "            on  t.techniqueId = b.techniqueId \r\n"
          + "            and t.techniqueName = ''{12}''\r\n"
          + "    where   b.{13} = ''{14}'';";

  /** */
  private static final long serialVersionUID = -623560886259020605L;

  @Getter private final JComboBox<Technique> techniqueSelector;
  @Getter private final JComboBox<Long> oemSelector;
  @Getter private final JComboBox<T> itemSelector;
  private final JButton loadDb, loadFile, saveFile, insertSql, updateSql;
  @Getter private final RSyntaxTextArea jsCodeEditor, dependenciesField;
  @Getter private final JTextField intervalField;
  @Getter private final JCheckBox filterCheckbox;

  private File lastFolder = new File(System.getProperty("user.dir"));

  private final List<IDataChangeListener> dataChangeListeners = new ArrayList<>();

  protected final ConnectionManager cm;
  protected final Connect connect;

  public Formulas(ConnectionManager connectionManager, Connect connect) {
    this.cm = connectionManager;
    this.cm.addListener(this);

    this.connect = connect;

    intervalField = new JTextField("1");

    filterCheckbox = new JCheckBox("Filter items for existing formulas");
    filterCheckbox.addActionListener(
        a -> {
          new Thread(() -> SwingUtilities.invokeLater(() -> Base.withDb(() -> filterFormulas())))
              .start();
        });
    filterCheckbox.setSelected(true);

    itemSelector = new JComboBox<>();
    techniqueSelector = new JComboBox<>();
    oemSelector = new JComboBox<>();

    techniqueSelector.addActionListener(a -> {
      new Thread(() -> {
        SwingUtilities.invokeLater(() -> {
          Base.withDb(() -> filterOems());
        });

      }).start();
    });

    oemSelector.addActionListener(a -> {
      new Thread(() -> {
        SwingUtilities.invokeLater(() -> {
          Base.withDb(() -> filterFormulas());
        });

        updateListeners((Technique) techniqueSelector.getSelectedItem());
      }).start();
    });

    dependenciesField = new RSyntaxTextArea();
    dependenciesField.setTabSize(3);
    dependenciesField.setCaretPosition(0);
    dependenciesField.requestFocusInWindow();
    dependenciesField.setMarkOccurrences(true);
    dependenciesField.setCodeFoldingEnabled(true);
    dependenciesField.setClearWhitespaceLinesEnabled(false);
    dependenciesField.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JSON);
    dependenciesField.setHighlightCurrentLine(false);
    dependenciesField.setRows(3);
    dependenciesField.setText("plus({type: \"timeSeries\", name:\"Tiefe\"}, 2)");

    jsCodeEditor = new RSyntaxTextArea();
    jsCodeEditor.setTabSize(3);
    jsCodeEditor.setCaretPosition(0);
    jsCodeEditor.requestFocusInWindow();
    jsCodeEditor.setMarkOccurrences(true);
    jsCodeEditor.setCodeFoldingEnabled(true);
    jsCodeEditor.setClearWhitespaceLinesEnabled(false);
    jsCodeEditor.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_JAVASCRIPT);

    loadDb = new JButton("Load Formula from DB");
    loadDb.addActionListener(
        a -> {
          getIdAndLoad();
        });

    loadFile = new JButton("Load Formula from File");
    loadFile.addActionListener(a -> loadFromFile());

    saveFile = new JButton("Save to File");
    saveFile.addActionListener(a -> saveToFile(jsCodeEditor.getText(), "JavaScript", "js"));

    insertSql = new JButton("Save as Insert SQL");
    insertSql.addActionListener(a -> saveInsertSql());
    insertSql.setEnabled(false);

    updateSql = new JButton("Save as Update SQL");
    updateSql.addActionListener(a -> saveUpdateSql());
    updateSql.setEnabled(false);

    itemSelector.addActionListener(
        a -> {
          boolean buttonEnable = itemSelector.getSelectedItem() != null;

          updateSql.setEnabled(buttonEnable);
          insertSql.setEnabled(buttonEnable);
        });

    build();
  }

  protected abstract Object filterOems();

  protected abstract void saveUpdateSql();

  protected abstract void saveInsertSql();

  protected void saveToFile(String text) {
    saveToFile(text, "SQL", "sql");
  }

  private void saveToFile(String text, String description, String extension) {
    JFileChooser jf = new JFileChooser(lastFolder);
    jf.setFileFilter(
        new javax.swing.filechooser.FileFilter() {

          @Override
          public String getDescription() {
            return description;
          }

          @Override
          public boolean accept(File f) {
            return f.getName().endsWith("." + extension) || f.isDirectory();
          }
        });
    int selection = jf.showSaveDialog(null);

    File selectedFile = jf.getSelectedFile();
    if (selection != JFileChooser.CANCEL_OPTION && selectedFile != null) {
      lastFolder = selectedFile.getParentFile();
      try {
        if (!selectedFile.getName().endsWith(extension)) {
          selectedFile = new File(selectedFile.getAbsolutePath() + "." + extension);
        }
        Files.writeString(selectedFile.toPath(), text, StandardCharsets.UTF_8);

        JOptionPane.showMessageDialog(
            null,
            "Saved file to: " + selectedFile.getAbsolutePath(),
            "Saved",
            JOptionPane.INFORMATION_MESSAGE);

      } catch (IOException e) {
        log.error(e.getMessage(), e);
      }
    }
  }

  private void updateListeners(Technique technique) {
    if (technique != null) {
      dataChangeListeners.forEach(listener -> listener.dataChanged(technique));
    }
  }

  private void getIdAndLoad() {
    @SuppressWarnings("unchecked")
    Long parameterId = ((T) itemSelector.getSelectedItem()).getLongId();

    if (parameterId != null) {
      Base.withDb(() -> loadFromDb(parameterId));
    }
  }

  protected abstract Object filterFormulas();

  protected abstract Object loadFromDb(long id);

  private void loadFromFile() {
    JFileChooser jf = new JFileChooser(lastFolder);
    int selection = jf.showOpenDialog(null);

    File selectedFile = jf.getSelectedFile();
    if (selection != JFileChooser.CANCEL_OPTION && selectedFile.isFile()) {
      lastFolder = selectedFile.getParentFile();
      try {
        String jsScript = Files.readString(selectedFile.toPath());

        jsCodeEditor.setText(jsScript);
      } catch (IOException e) {
        log.error(e.getMessage(), e);
      }
    }
  }

  private void build() {
    removeAll();

    setLayout(new BorderLayout(5, 5));

    JPanel left = new JPanel(new GridLayout(5, 1));
    left.add(new JLabel("Technique: "));
    left.add(new JLabel("OEM: "));
    left.add(new JLabel(String.format("%s: ", getItemName())));
    left.add(new JLabel("Interval:"));

    JPanel right = new JPanel(new GridLayout(5, 1));
    right.add(techniqueSelector);
    right.add(oemSelector);
    right.add(itemSelector);
    right.add(intervalField);
    right.add(filterCheckbox);

    JPanel buttons = new JPanel(new GridLayout(1, 5));
    buttons.add(loadDb);
    buttons.add(loadFile);
    buttons.add(saveFile);
    buttons.add(insertSql);
    buttons.add(updateSql);

    JPanel top = new JPanel(new BorderLayout(5, 5));
    top.add(left, BorderLayout.WEST);
    top.add(right, BorderLayout.CENTER);
    top.add(buttons, BorderLayout.SOUTH);

    this.add(top, BorderLayout.NORTH);

    RTextScrollPane sp = new RTextScrollPane(jsCodeEditor);
    this.add(sp, BorderLayout.CENTER);

    RTextScrollPane sp2 = new RTextScrollPane(dependenciesField);
    this.add(sp2, BorderLayout.SOUTH);

    customBuild();

    validate();
    repaint();
  }

  protected abstract void customBuild();

  protected abstract String getItemName();

  @Override
  public void connected() {
    techniqueSelector.removeAllItems();
    oemSelector.removeAllItems();

    Base.withDb(
        () -> {
          Technique.findAll()
              .forEach(
                  technique -> {
                    techniqueSelector.addItem((Technique) technique);
                  });

          addOems();
          return null;
        });
  }

  protected abstract void addOems();

  @Override
  public String getJsFormula() {
    return dependenciesField.getText();
  }

  public void addDataChangeListener(IDataChangeListener dataChangeListener) {
    this.dataChangeListeners.add(dataChangeListener);
  }

  @Override
  public double getInterval() {
    String number = intervalField.getText();
    if (NumberUtils.isParsable(number)) {
      return Double.parseDouble(number);
    }
    return 1.0;
  }

  @Override
  public boolean asMap() {
    return false;
  }


  @Override
  public RSyntaxTextArea getFullFormulaField() {
    return this.jsCodeEditor;
  }

  @Override
  public RSyntaxTextArea getFormulaField() {
    return this.dependenciesField;
  }
}
