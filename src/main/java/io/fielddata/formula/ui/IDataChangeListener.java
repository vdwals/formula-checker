package io.fielddata.formula.ui;

import io.fielddata.formula.domain.Technique;

public interface IDataChangeListener {

  void dataChanged(Technique technique);
}
