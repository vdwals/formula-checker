package io.fielddata.formula.ui;

public interface IDataProvider {
  Integer getElementId();

  Integer getTechniqueId();

  Integer getSourceId();
}
