package io.fielddata.formula.ui;

import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

public interface IFormulaProvider {
  String getJsFormula();

  RSyntaxTextArea getFullFormulaField();

  RSyntaxTextArea getFormulaField();

  double getInterval();

  boolean asMap();
}
