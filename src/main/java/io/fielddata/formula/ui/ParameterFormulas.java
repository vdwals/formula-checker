package io.fielddata.formula.ui;

import io.fielddata.formula.connection.ConnectionManager;
import io.fielddata.formula.domain.Parameter;
import io.fielddata.formula.domain.ParameterFormula;
import io.fielddata.formula.domain.Technique;
import java.text.MessageFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.ObjectUtils;
import org.javalite.activejdbc.Base;

public class ParameterFormulas extends Formulas<Parameter> {

  private static final long serialVersionUID = 7727665991551660705L;

  public ParameterFormulas(ConnectionManager connectionManager, Connect connect) {
    super(connectionManager, connect);
  }


  @Override
  protected Object filterOems() {
    Technique selected = (Technique) getTechniqueSelector().getSelectedItem();
    getOemSelector().removeAllItems();

    if (selected != null) {
      List<Long> ids = selected
          .get(Parameter.class, "valid = ? or projectId is not null", 1)
          .stream()
          .map(Parameter::getLongId)
          .collect(Collectors.toList());

      ParameterFormula
          .findByParamIds(ids)
          .stream()
          .map(ParameterFormula::getOemId)
          .collect(Collectors.toSet())
          .forEach(oemId -> getOemSelector().addItem(oemId));

    }
    return null;
  }

  @SuppressWarnings("unchecked")
  @Override
  protected Object filterFormulas() {
    Technique selectedTechnique = (Technique) getTechniqueSelector().getSelectedItem();
    Long selectedOemId = (Long) getOemSelector().getSelectedItem();
    getItemSelector().removeAllItems();

    if (selectedTechnique != null && selectedOemId != null) {
      List<Parameter> parameters =
          selectedTechnique.get(Parameter.class, "valid = ? or projectId is not null", 1);

      if (getFilterCheckbox().isSelected()) {
        List<Long> ids = parameters.stream().map(Parameter::getLongId).collect(Collectors.toList());
        parameters =
            ParameterFormula
                .findByParamIdsAndOemId(ids, selectedOemId)
                .include(Parameter.class)
                .stream()
                .map(
                    parameterFormula ->
                        ((ParameterFormula) parameterFormula).parent(Parameter.class))
                .collect(Collectors.toList());
      }

      parameters
          .stream()
          .sorted(Comparator.comparing(Parameter::toString))
          .forEach(parameter -> getItemSelector().addItem(parameter));
    }
    return null;
  }

  @Override
  protected Object loadFromDb(long id) {
    ParameterFormula parameterFormula = ParameterFormula.findById(id);

    if (parameterFormula != null) {
      String jsScript = parameterFormula.getFormula();

      String dependencies = parameterFormula.getDependencies();

      if (jsScript != null) {
        getJsCodeEditor().setText(jsScript);
      }
      if (dependencies != null) {
        getDependenciesField().setText(dependencies);
      }
    } else {
      System.err.println(String.format("No parameter formula for ID %s found", id));
    }
    return null;
  }

  @Override
  protected String getItemName() {
    return "Parameter";
  }

  @Override
  protected void saveUpdateSql() {
    Parameter selectedItem = (Parameter) getItemSelector().getSelectedItem();
    Technique selectedTechnique = (Technique) getTechniqueSelector().getSelectedItem();

    if (ObjectUtils.allNotNull(selectedItem, selectedTechnique)) {
      String update =
          MessageFormat.format(
              UPDATE_SQL,
              new Date(),
              connect.getConfig().getDatabase(),
              ParameterFormula.getTableName(),
              Parameter.getTableName(),
              selectedItem.getIdName(),
              ParameterFormula.metaModel().getIdName(),
              Parameter.TEMPLATE_KEY,
              selectedItem.getTemplateKey(),
              selectedTechnique.getName(),
              ParameterFormula.FORMULA,
              getJsCodeEditor().getText(),
              ParameterFormula.DEPENDENCIES,
              getDependenciesField().getText(),
              ParameterFormula.OEM_ID,
              getOemSelector().getSelectedItem());
      saveToFile(update);
    } else {
      System.err.println("No parameter selected.");
    }
  }

  @Override
  protected void saveInsertSql() {
    Parameter selectedItem = (Parameter) getItemSelector().getSelectedItem();
    Technique selectedTechnique = (Technique) getTechniqueSelector().getSelectedItem();

    if (ObjectUtils.allNotNull(selectedItem, selectedTechnique)) {
      String insert =
          MessageFormat.format(
              INSERT_SQL,
              new Date(),
              connect.getConfig().getDatabase(),
              ParameterFormula.getTableName(),
              ParameterFormula.metaModel().getIdName(),
              ParameterFormula.OEM_ID,
              ParameterFormula.FORMULA,
              ParameterFormula.DEPENDENCIES,
              selectedItem.getIdName(),
              getOemSelector().getSelectedItem(),
              getJsCodeEditor().getText(),
              getDependenciesField().getText(),
              Parameter.getTableName(),
              selectedTechnique.getName(),
              Parameter.TEMPLATE_KEY,
              selectedItem.getTemplateKey());

      saveToFile(insert);
    } else {
      System.err.println("No parameter selected.");
    }
  }

  @Override
  protected void addOems() {
    Base.withDb(
        () -> {
          ParameterFormula
              .findAll()
              .stream()
              .map(parameterFormula -> (ParameterFormula) parameterFormula)
              .map(ParameterFormula::getOemId)
              .collect(Collectors.toSet())
              .forEach(oemId -> getOemSelector().addItem(oemId));

          if (getOemSelector().getItemCount() > 0) {
            getOemSelector().setSelectedIndex(0);
          }
          return null;
        });
  }

  @Override
  protected void customBuild() {
    getIntervalField().setEnabled(false);
  }
}
