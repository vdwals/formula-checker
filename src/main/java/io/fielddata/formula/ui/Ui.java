package io.fielddata.formula.ui;

import io.fielddata.formula.config.Configs;
import io.fielddata.formula.connection.ConnectionManager;
import io.fielddata.formula.engine.Engine;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class Ui extends JPanel {
  private static final long serialVersionUID = 883179598603802062L;

  private final Connect connect;
  private final Data data;
  private final ParameterFormulas parameterFormula;
  private final DepthFormulas depthFormula;
  private final Eval evalParam, evalDepth;
  private final Console console;

  public Ui(Configs configs, ConnectionManager connectionManager, Engine engine) {
    connect = new Connect(configs, connectionManager);
    parameterFormula = new ParameterFormulas(connectionManager, connect);
    depthFormula = new DepthFormulas(connectionManager, connect);
    data = new Data(connectionManager);

    parameterFormula.addDataChangeListener(data);
    depthFormula.addDataChangeListener(data);

    evalParam = new Eval(parameterFormula, data, engine);
    evalDepth = new Eval(depthFormula, data, engine);

    console = new Console();

    build();
  }

  private void build() {
    removeAll();

    this.setLayout(new BorderLayout(5, 5));

    JTabbedPane tab = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);

    //    JSplitPane paramMain = new JSplitPane(JSplitPane.VERTICAL_SPLIT, parameterFormula,
    // evalParam);
    JPanel paramMain = new JPanel(new BorderLayout(5, 5));
    paramMain.add(parameterFormula, BorderLayout.CENTER);
    paramMain.add(evalParam, BorderLayout.SOUTH);

    tab.addTab("Parameter", paramMain);

    JPanel depthMain = new JPanel(new BorderLayout(5, 5));
    depthMain.add(depthFormula, BorderLayout.CENTER);
    depthMain.add(evalDepth, BorderLayout.SOUTH);

    //    JSplitPane depthMain = new JSplitPane(JSplitPane.VERTICAL_SPLIT, depthFormula, evalDepth);

    tab.addTab("Depth", depthMain);
    tab.setBorder(BorderFactory.createTitledBorder("2. Select Formula"));
    this.add(tab, BorderLayout.CENTER);

    JPanel left = new JPanel(new BorderLayout(5, 5));

    JPanel top = new JPanel(new BorderLayout(5, 5));
    connect.setBorder(BorderFactory.createTitledBorder("1. Connection details"));
    top.add(connect, BorderLayout.NORTH);

    data.setBorder(BorderFactory.createTitledBorder("3. Data source"));
    top.add(data, BorderLayout.SOUTH);

    left.add(top, BorderLayout.NORTH);
    left.add(console, BorderLayout.CENTER);

    this.add(left, BorderLayout.WEST);
    this.setVisible(true);
  }
}
